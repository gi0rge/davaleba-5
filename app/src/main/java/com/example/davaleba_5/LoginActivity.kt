package com.example.davaleba_5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var passwordRepeat: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        email = findViewById(R.id.userEmail)
        password = findViewById(R.id.password)
        passwordRepeat = findViewById(R.id.passwordRepeat)

    }
    fun checkReg(view: View) {
        val passText = password.text.toString()
        val emailText = email.text.toString()
        val passRepeatText = passwordRepeat.text.toString()

        if("@" !in emailText || "." !in emailText)  {
            val message = "ასეთი e-mail არ არსებობს"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }
        else if(password.text.trim().length < 9 || Regex("(?=.*[0-9])") !in passText)  {
            val message = "პაროლი უნდა შედგებოდეს მინიმუმ 9 სიმბოლოსგან და მოიცავდეს მინიმუმ ერთ ციფრს"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }

        else if(passRepeatText != passText)  {
            val message = "პაროლები არ ემთხვევა ერთმანეთს"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }


        FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailText, passText)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    val message = "აუტენტიფიკაცია წარმატებით დასრულდა"
                    val intent = Intent(this, ProfileActivity::class.java)
                    startActivity(intent)
                    Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    Toast.makeText(applicationContext, "an error occurred", Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun gotoProfile() {


    }

}